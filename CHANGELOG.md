# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/seshadri.aadithya92/changelogtest/compare/v1.1.1...v1.2.0) (2021-06-22)


### Features

* add music lines! ([fbfa9cd](https://gitlab.com/seshadri.aadithya92/changelogtest/commit/fbfa9cdbf48582d88a0d394e363d17eec7791c2d))

### [1.1.1](https://gitlab.com/seshadri.aadithya92/changelogtest/compare/v1.1.0...v1.1.1) (2021-06-22)

## 1.1.0 (2021-06-22)


### Features

* add standard release support! ([a422658](https://gitlab.com/seshadri.aadithya92/changelogtest/commit/a422658b636710c7a2b81e8ef0e3c0b1f51caeeb))


### Bug Fixes

* create nodejs application ([d931e6b](https://gitlab.com/seshadri.aadithya92/changelogtest/commit/d931e6bb91dfed609af68d63582a701ddd18072e))
